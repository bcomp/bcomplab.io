---
name: Legged Balancing Robot
tools: [robotics, controlsystems, simulation, masters thesis]
image: /assets/mdimg/LSBR_Cover.jpg
category: Academic
special_category: true
description: Control strategy analysis and generalized modeling of a novel, self-balancing robotic mechanism.
title: Legged Self-Balancing Robot
external_url: http://hdl.handle.net/10211.3/219599
cardorder: 1
---
