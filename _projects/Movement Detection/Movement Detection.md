---
name: Visual Movement Detection
tools: [imageprocessing, matlab]
image: /assets/mdimg/ImageTracking_Cover.jpg
category: Academic
timeframe: Spring 2017
description: Isolating moving objects in video stream using low-level image processing operations.
title: Motion Detection Using Cellular Image Processing Primitives
cardorder: 4
---

![](mdimg/TopVisual-Detect.jpg)

---

Computer vision (CV) has evolved into a broad topic, ranging from pixel-level optical processing to sophisticated, high-level applications of machine learning.  This project was part of a course called _Image Processing_, which primarily concerned the fundamentals of _low-level_ algorithmic manipulation and analysis of image data, that are used for both high-speed, application-specific tasks and to compose stages in more complex CV pipelines.

The goal of the project was to create a minimal algorithm that worked in a limited - yet well-defined environment, and could be practically useful in certain commercial settings. 

Given a video of a moving object within the controlled environment, the algorithm could successfully _isolate the object_ regardless of shape or surface roughness, and _generate normalized position data_ over time, using only basic image processing operations, which are described in more detail below.

> <i class="fa fa-file-powerpoint-o"></i>&nbsp; [In-class Presentation Slides](ProjectPresentation_Companeitz.pdf) /&nbsp;<i class="fa fa-file-pdf"></i>&nbsp; [Written Report](ECE595_ProjectReport_Companeitz.pdf)

---

## Well-defined Problem

The first and most important step in the project was clearly defining a _limited problem domain_, since real applications of image processing in _biomedical engineering_, _manufacturing_, and other such fields often work to ensure consistent measurement environments, regardless of the technology.

The video footage that this program was intended to process had a set of simple requirements, reminiscent of a controlled measurement environment: 

- A featureless background
- Desired object is the largest in the frame
- Object has at least one rotational axis that is fixed, with respect to the camera
- The camera has a relatively narrow field of view

The object could have smooth or ragged edges and there could be significant internal complexity within the boundaries of the object, but none of this should affect detection.  Below are two examples of items used in the test videos, that were suspended on a transparent fishline and moved in all directions, throughout the camera viewport.

<figure class="figure w-100">
<div class="row">
<div class="col-md-6">
  <img src="mdimg/lu96560vpy929_tmp_ba3cdaff8e1c2ba3.gif" alt="img" />
</div> <div class="col-md-6">
  <img src="mdimg/lu96560vpy929_tmp_de201702b2281cb2.gif" alt="img" />
</div> </div>
  <figcaption class="figure-caption text-center">Figure 1: Two frames from test videos filmed for the project; objects are moved through the frame on transparent fishing line.</figcaption>
</figure>

## Detection Strategy

Object's pixels in a certain frame can be represented by what is called a [binary segmentation](https://en.wikipedia.org/wiki/Image_segmentation) image, where each pixel is either zero (black) or one (white), indicating that this pixel is a part of the object.  Being able to segment the original video frames and provide some analytical feedback constitutes "detection" in this context.

Essentially, this boiled down to three steps: 1) isolating the object from the rest of the image, 2) determining the object’s size, 3) and marking its position within the frame.  

### Isolating Objects

Because the object and background were significant constrained, we used simple image enhancement techniques to prepare each frame for binary segmentation via a type of intentional color degradation called [thresholding](https://en.wikipedia.org/wiki/Thresholding_(image_processing)).

When applied to areas in an image with very smooth edge transitions (such as a shadow) large [neighborhood smoothing filters](https://en.wikipedia.org/wiki/Gaussian_blur) produce false contouring, which creates edges within the smooth area that are thin enough to be ignored by the segmentation operator.  An 11x11 Gaussian filter was applied as preprocessing to the image to achieve the effect in Figure 2.

<figure class="figure w-100">
  <img src="mdimg/InducedContour.png" class="figure-img img-fluid rounded" alt="InducedContour" />
<figcaption class="figure-caption text-center">Figure 2: Induced false contouring of object's shadow</figcaption>
</figure>

### Morphological Segmentation

In order to isolate edges, images are often subjected what is called a [Sobel operator](https://en.wikipedia.org/wiki/Sobel_operator) that returns the derivative color difference between adjacent pixels.  Of course, most real-world objects often do not yield crisp, continueous edges, but clusters of edges at the boundary of the object.

To identify these zones and ignore edges resulting from spurious noise or specks in the image, two morphological functions can be chained together to identify the object: [dilation](https://en.wikipedia.org/wiki/Dilation_(morphology)#Grayscale_dilation) followed by [erosion](https://en.wikipedia.org/wiki/Erosion_(morphology)#Grayscale_erosion).  By using a smaller structured element for the dilation than for erosion, only regions which have clusters of edges remain.

<figure class="figure w-100">
  <img src="mdimg/lu96560vpy929_tmp_f42ccdccef927ed7.gif" alt="img" />
<figcaption class="figure-caption text-center">Figure 3: (Top-Left) Original, (Top-Right) Sobel Edge Filter, (Bottom-Right) Dilation, (Bottom-Left) Dilation Followed by Erosion</figcaption>
</figure>

After running the image through all the previous operations, the resulting image (above, bottom-left in our example) has been segmented to a useful point where we can apply the tracking logic.  Despite some inconsistency near the center of the object, the outer boundary remains consistent over multiple frames and can be used to identify our object as the largest item on screen and use it for tracking motion. 

### Motion Tracking Methodology

Now having a properly segmented image, an array must be built up containing all the connected white pixels that constitute our object.  This can be achieved using the [quadtree search algorithm](https://en.wikipedia.org/wiki/Quadtree), which has an efficient implementation within MATLAB called `bwconncomp` (which runs significantly faster than interpreted m-function implementations) that generates arrays of all contiguous pixel clusters.

Given such an array of pixels, the object's relative size and position could be found with 4 simple look-up commands, because we know how these pixels are ordered.  Quite simply, the difference of the first and last element of the pixel positions is the height of the object; similarly, the same calculation for the matrix transposed image gives the width.  The midpoint pixel of the array roughly represents the object center, but inconsistent object-internal segmentation can distort its accuracy.

<figure class="figure w-100">
<div class="row">
<div class="col-md-6">
  <img src="mdimg/lu96560vpy929_tmp_aaafe70314c3eca7.gif" alt="img" style="valign:bottom" /> 
  <figcaption class="figure-caption text-center">Original Object, First (green) and last (red) pixels</figcaption>
</div> <div class="col-md-6">
  <img src="mdimg/lu96560vpy929_tmp_8133053e7e18b2b2.gif" alt="img" />
  <figcaption class="figure-caption text-center">Transposed Image to determine Width</figcaption>
</div> </div>
  <figcaption class="figure-caption text-center">Figure 4: Simplest technique to determine rough object area</figcaption>
</figure>

Because we assumed that a certain dimension – the height, for instance – remained fixed, it was possible to determine a relative distance from the camera over multiple frames, by comparing these heights.  It is important to note that a single camera can only measure _relative_ size and distance over time, because there is no absolute depth information in a 2D image.

Although the camera frame is a rectangular grid, light arrives at the sensor in a radial pattern, so an object that maintains the same pixel size while moving across the screen would actually be following a curved trajectory!  Fortunately, the small viewing angle assumption allowed us to use the [small-angle assumption](https://en.wikipedia.org/wiki/Small-angle_approximation) and correlate size with distance linearly to get suitable results.

## Visualizing Results

Two graphical formats were selected that gave a balance of close-to-real-time performance and clear informational gain.  A cornered bounding box overlay was superimposed on the incoming video frames, and a 3D plot showing the overall path so far, which included a subtle "shadow plot" so as to more easily interpret the relative distance data, were all generated for test videos.

<figure class="figure w-100">
  <img src="mdimg/lu96560vpy929_tmp_959f6df73b389073.png" alt="img" /> 
<figcaption class="figure-caption text-center">Figure 5: (Left) Grayscale frame, Segmentation, and Tracking Box, (Right) 3D Plot with Shadow</figcaption>
</figure>

<figure class="figure w-100">
  <img src="mdimg/TotalVisualization.png" alt="TotalVisualization" /> 
<figcaption class="figure-caption text-center">Figure 6: Similar to Figure 5, but video included more front-back movement and used an object with a hole</figcaption>
</figure>

Except for some spurious size measurements as test objects entered or exited the frame, the program yielded convincing 3D path data for the movement of the objects in all videos tested.

## Final Thoughts

Video processing can be computationally feasible for limited computing systems, given a controlled environment.  Even in its limited state, several potential applications for this project already exist, such as parking garage car-counting, industrial conveyor-belt item identification, or even high-altitude drone avoidance.  The project provided a set of fundamental base-lines for simple motion processing, each of which can easily be enhanced by more sophisticated techniques in the future.
