---
name: Neuromuscular Junction Model
tools: [biomedical, simulation, matlab]
image: /assets/mdimg/NMJ_Cover.jpg
category: Academic
mathjax: true
description: Modeling the neuro-motive signal pathway from action potential to muscular Calcium absorption.
title: Modeling the Neuromuscular Junction
cardorder: 2
---

![](mdimg/TopVisual-NMJ.jpg)

---

Physical movement is essential to fitness and wellbeing, yet this basic ability is diminished for around one hundred thousand people globally who suffer persistent neurological diseases, and at least twice that who will require lower extremity amputation every year. [^1] [^2]  In order to begin to understand what signal and control problems can arise in skeletal physiology, it is essential to study where our neurological signals and muscle cells intersect in the human body: _the neuro-muscular junction (NMJ)._

This project was part of a graduate-level _biomedical engineering_ course, the goal of which was to model and simulate the multi-stage pathway of electrochemical signals across the NMJ, from the final transmitting neuron to the first receiving muscle cell.  All mathematical models from the relevant literature and numerical methods to perform the simulation were manually implemented in _MATLAB_.

_Hopefully this article will provide the interested reader with a meaningful overview of the pathway by which signals transfer over from our nervous system into our muscles._

> <i class="fa fa-file-pdf"></i>&nbsp; [IEEE-Formatted Project Report](ModelingNMJReport_Companeitz.pdf) 

---

## What is the Neuromuscular Junction?

The neuromuscular junction (NMJ) is simply the location where nerve voltage potentials are converted into the ionic movement and chemical signaling necessary to activate muscle contraction. [^3]

In fact, the NMJ has been the subject of significant microbiological research since the late 1950s with a series of papers studying synaptic physiology using _in vitro_ frog motor axons.  This was around the time the foundational Hodgkin-Huxley mathematical neuron model had begun to gain popularity - which was itself based on studies of squid motor axons. [^4] 

<figure>
<img src="mdimg/NMJIllustration.png" alt="nmj" style="width:75%;" />
<figcaption class="figure-caption text-center" align="center">
 Connection of a single nerve and muscle cell.  <a href="https://biologydictionary.net/neuromuscular-junction/">Image Source</a>
</figcaption>
</figure>

Although many accepted techniques exist to represent and simulate the constituent processes within the NMJ - such as electrochemical potentials, vesicular release, chemical diffusion, and so on - there is currently no single model linking neuronal action potential to length-wise muscle contraction.  

As such, this paper approached understanding the overall behavior constructively, by composing the models together to form a pathway of intermediate mechanisms.  This means we must follow the pathway of processes by which our signal travels through the walls between cells.

## Tracing the Pathway Through the NMJ

In most situations, muscles are stimulated by signals originating from the nervous system.  Motor neurons connect the central nervous system (CNS) to both smooth and skeletal muscles via neuron bundles in the spinal chord that branch out hierarchically to different muscle groups.  

Each neuron cell transmits information via a uni-directional ionic voltage wave called an _action potential_ that travels from the cell body and down the sometimes centimeter long axon cables to the next neuron and finally, the muscle cell.  It is at the terminus of the final neuron that our pathway begins.

<figure class="figure w-100">
<div class="row">
  <img src="mdimg/nmj.png" alt="nmj" style="float: left; width: 35%;" />
  <img src="mdimg/nmj_tlbd.png" alt="nmj_tlbd" style="float: left; width: 65%;"/>
</div>
  <figcaption class="figure-caption text-center" align="center">Signal pathway block diagram from neuron to muscle cell (color-coded). <a href="https://en.wikipedia.org/wiki/Neuromuscular_junction">(Left) Image Source</a></figcaption>
</figure>

Similarly to neuron-neuron synapses, neuron-muscle intercellular communication is mitigated by a neurotransmitter, which is a molecule emitted by the _presynaptic_ cell that stimulates membrane depolarization in the _postsynaptic_ cell membrane. [^5]  In the case of the NMJ, the molecule is _acetylcholine_ (abrv. $ACh$), which can act as both a neurotransmitter and neuromodulator and is found throughout the body in both the autonomic and somatic nervous systems.

Once this transmitter chemical diffuses across the NMJ gap junction and is absorbed by the muscle cell's neuroreceptors, the pathway is complete, for the purposes of this investigation.

### Axon Terminal Bouton

First, action potentials from the final neuron motor axon arrive at the terminal bouton, which is the disk-shaped, presynaptic portion of the neuron at the end of the axon, consisting of tightly packed calcium ion channels and stored neurotransmitters in long rows next to each other.  As the voltage in the terminal changes, it rapidly activates these _voltage-gated calcum channels_ (VGCCs), allowing extracellular calcium to enter the cell, triggering specialized SNARE proteins. [^6] [^7]  

<figure>
<img src="mdimg/AxonTerminal.png" alt="AxonTerminal" style="width:70%;" /> 
<figcaption class="figure-caption text-center" align="center">Organization of calcium channels and vesicles in the axon terminal.  Image Source: [7] </figcaption>
</figure>

These proteins react dramatically as they are flooded with Calcium, by fusing the nearby, membranous bubbles of acetylcholine (called *vesicles*) into the neuron membrane, releasing their contents outside the neuron, into the gap between cells.  This process is called _vesicular excytosis_, and the gap itself is generally called the _synaptic_ or _neuromuscular cleft_.

### Neuromuscular Cleft

Just a single vesicle releases a cluster of around 10,000 neurotransmitter molecules into the cleft, where they diffuse through a 50nm wide region of interstitial fluid between the presynaptic neuron membrane and postsynaptic muscle cell membrane.  While traveling, the neurotransmitter is actively broken down by an enzyme that is uniformly present in the cleft that reduces the amount of transmitter that can make it to the muscle cell surface, but also prevents long-term transmitter saturation and even recycles it back into the neuron.

<figure>
<img src="mdimg/SarcolemmaRidges.png" alt="SarcolemmaRidges" style="width:50%;" /> 
<figcaption class="figure-caption text-center" align="center"> Simplified 3D visualization of NMJ cleft volume, including ridge formation on the muscle membrane (post-synaptic). Image Source: [14]</figcaption>
</figure>

### Muscle Membrane Neuroreception

All the acetylcholine transmitter molecules that make it to the sarcolemma (muscle cell wall) bind to closely-distributed neuroreceptors, which are ion channels that only open when bound to a neurotransmitter.  Specifically, nicotinic acetylcholine receptors ($nAChR$) bind with two acetylcholine molecules ($ACh$) and allow Sodium to pass through the sarcolemma, causing an ionic depolarization in the muscle cell.  Often referred to as the endplate potential (EPP), this new voltage change in the muscle is the signal that ultimately triggers contraction.

<figure>
<img src="mdimg/NicotinicReceptors.png" alt="NicotinicReceptors" style="width:75%;" /> 
<figcaption class="figure-caption text-center" align="center"> Nicotinic acetylcholine-gated channel illustration showing double binding neccessary to permit sodium into cell. <a href="https://www.uprm.edu/inqu/che-seminar-series-nicotinic-acetylcholine-receptors-in-disease-from-structure-to-therapeutic-strategies-dr-jose-lasalde-upr-rp-vp-research-and-technology/">Image Source</a></figcaption>
</figure>

## Modeling the Pathway

The behavior of each stage in the pathway can be represented by math models found in literature.  It is important to remember that these models are all based on study of *in vitro* analysis, so *in vivo* behavior may differ slightly.

For the more math and chemistry averse reader, this section will be fairly formula and reaction heavy because it outlines the entire basis for the simulation.  There were more detailed equations involved, but the goal here is to skim over numerical analysis concerns and focus on the fundamental microbiological representations. 

### Axon Modified Hodgkin-Huxley Model

The beauty of the Hodgkin-Huxley model (HHM) is the ease of including additional ion channels, by simply adding a current term to the existing membrane current relation, shown below

$$
I_m = C_m \frac{dV_m}{dt} + I_{Na} + I_{K} + I_L + \mathbf{I_{Ca}}
$$

This new ion current is dependent on the the current membrane voltage and the gating coefficients, $p$ and $q$, which is the same representation as the HHM.

$$
I_{CaN} = g_{CaN} p^2 q (V_m - V_{Ca})
$$

What differs is the form of the gating parameter derivatives, which are usually presented in what is called *Boltzmann form* involving $\alpha$ and $\beta$ coefficients, but were instead provided as a relationship of (in)activation asymptotic functions $p_{\ast,\infty}(V_m)$ and a time constant $\tau_{\ast}$. [^8]

$$
\frac{dp}{dt} = \frac{p_{ac,\infty}(V_m) - p}{\tau_{ac}} ,\quad \frac{dq}{dt} = \frac{p_{in,\infty}(V_m) - q}{\tau_{in}}
$$

Apparently it is possible to convert between this form and the classic Boltzmann form (not shown), but the numerical result will turn out the same either way.

### Nernst Sigmoid Vesicle Release Model

The most recent work on determining the calcium release relationship (CRR) shows that the process is likely at fourth order process that depends on both calcium and magnesium agonism. [^9]

However, during *normal* nerve transductance, magnesium has little effect, so it is possible to use a simpler technique called the *Nernst sigmoid model* to link the concentration of *liberated acetylcholine* (called $L$ here) to the presynaptic voltage at the axon. [^10]

$$
[L](V_{pre}) = \frac {L_{max}}{1+exp[-(V_{pre}-V_p)/K_p]}
$$

In this equation $L_{max}$ is the maximal concentration of neurotransmitter, $V_p$ is the functional "half-activation" point, and $K_p$ is the steepness of the function curve, all values of which are determined by _least-squares estimation_ of experimental data.  We scale this concentration to a single vesicle by generating $L_{max}$ based on average vesicular molecule count in literature within the area of one "pixel" in our 2D diffusion grid.

This model for acetylcholine concentration is coupled with the reaction kinetics from the same paper.

$$
\begin{gathered}
4Ca^{2+} + X \overset{k_b}{\underset{k_u}{\rightleftharpoons}} X^\ast \\
X^\ast + V_e \overset{k_1}{\underset{k_2}{\rightleftharpoons}} V_e^\ast \overset{k_3}{\rightarrow} n L
\end{gathered}
$$

Calcium ions bind with a "calcium-binding protein" $X$ (since discovered to be synaptatagomin) and then the vescicle $V_e$ to irreversibly release $n$ molecules of the liberated transmitter $L$, modeled as a sigmoidal concentration above. 

$$
[ACh](x,r,t) = \begin{cases}
\frac{d[L]}{dt} &, \frac{d[L]}{dt} \ge 0 \\
0 &,\frac{d[L]}{dt} \lt 0
\end{cases} \quad \text{when, } x = r = 0
$$

The piece-wise relationship shown above, is included to address the fact that the *negative-slope* portion of the Nernst model represents liberated transmitters *leaving* the test area, which we intend to model ourselves as a 2D diffusion model: at each time step, the concentration change ${d[L]}/{dt}$ is added into the diffusion grid.

### 2D Finite-Difference Diffusion Model

The cleft is represented as a two-dimensional grid of pixels on the $r$ and $t$ axes, each with its own time-dependent concentration of $ACh$, enzymes, and Michaelis-Mentis intermediate reactants. [^11] [^12]  To model the concentration gradients between adjacent pixels, first order time derivatives were calculated using forward finite difference technique, and the spatial second order derivatives were evaluated numerically using _midpoint_ finite differences, for edge-adjacent pixels, using "pass-through" boundary conditions for all edges, except the top presynaptic membrane, where no neurotransmitter was allowed to diffuse.

$$
\begin{gathered}
\frac{\partial A}{\partial t} = D_t \frac{\partial^2A}{\partial x^2} + D_r \left( \frac{\partial^2A}{\partial r^2} + \frac{1}{r} \frac{\partial A}{\partial r}\right) + F_e + (F_{r1} + F_{r2})\delta(x-w) \\
\end{gathered}
$$

Within each pixel, the functions $F_e, F_{r1},$ and $F_{r2}$ representing the enzymatic breakdown of $ACh$ are also calculated, based on the following, three-step reaction dynamics:

$$
\begin{gathered}A + E \overset{k_{E1}}{\underset{k_{-E1}}{\rightleftharpoons}} AE \\AE \overset{k_{E2}}{\rightarrow} acE \\acE \overset{k_{E3}}{\rightarrow} E\end{gathered}
$$

### Membrane-Distributed Neuroreceptor Model

The bottom postsynaptic row of pixels included additional reaction dynamics to represent the two-step activation of $nAChR$ receptors by diffused $ACh$ transmitter, shown below. 

$$
\begin{gathered}
A + R \overset{k_{R}}{\underset{k_{-R}}{\rightleftharpoons}} AR \\
AR + A \overset{k_{AR}}{\underset{k_{-AR}}{\rightleftharpoons}} A_2 R
\end{gathered}
$$

By summing together the individual open receptor concentrations along the length of the bottom row of pixels, we end up with a scalar concentration of active $nAChR$ receptors in our postsynaptic muscle cell. [^13]

$$
\text{nAChR open,total} = \sum^{len(r)}_{i=1} [nAChR_{open}]_i
$$

It is important to note this concentration is only an indicator of neuroreception for this specific region of the membrane.  Deriving the EPP voltage from this value would involve more modeling to account for the muscular ion channels that drive depolarization, a process that was not a part of this study.

## Simulation Results

A square wave pulse was applied to stimulate the model.  The simulation run below was run with a pulse of amplitude $V_{amp} = 74mV$ and start and stop times $t_u=0.2,\space t_d=0.4$.  Being implemented in _MATLAB_ on an mid-range laptop, the program took roughly $30sec$ real time to complete a single run!

<figure>
<img src="mdimg/TestbenchPlots1.png" width="75%">
<figcaption class="figure-caption text-center" align="center">Signal plots for square input single pulse experiment, in order of signal propagation (Top to Bottom)</figcaption>
</figure>

The simulation produced two products, a chart containing several plots at each stage up to (above, top 3 plots) and after the diffusion (above, bottom plot), and a cartoon of 8 snapshots in time of the diffusion medium for several states during peak acetylcholine release at $\approx 1.3 ms$ (shown below), where concentration is the color of the pixel, with yellow being the most, and dark blue being the least, as shown in the color bar of the last frame.  Around $1.2 ms$ elapse between the start of the nerve signal and peak neurotransmitter reception.

<figure>
<img src="mdimg/TestbenchDiffusion1cb.png" width="75%">
<figcaption class="figure-caption text-center" align="center">Diffusion for single pulse experiment, where position units are nanometers, and the color units are milliMolarity, indicated in the bottom right frame colorbar</figcaption>
</figure>

In the first two frames of the diffusion cartoon, a slight contouring near the bottom edge indicates that the acetylcholine moves slightly faster in a straight line to the post-synaptic cell, but mostly spreads evenly into the cleft.  It is possible modifying the simulation to capture the ridges typically found on the post-synaptic cell, or using a more dramatic colorbar scheme could reveal more about this artifact.

## Final Thoughts

Because the project ended with neuroreception concentration scaled to a single vesicle, clinical data relevant for comparison was quite limited.  However, this project has the potential to model all the way to post-synaptic membrane voltage and even contractile force, using several other research models that were not included for the purpose of time.  A model extended in these ways could then be compared more readily with experimental EPPs and force data.

Better understanding how signals traverse this junction improves not only our ability to diagnose how certain neurological disorders can manifest mechanically (e.g. aberrant muscle movements, fasciculations, hyporeflexia, etc,) but also potentially augment our ability to reverse engineer motor unit action potentials and more accurately mimic muscular responses for advanced prosthetics in the future!

## References

[^1]: W. H. Organization. (2006). Neurological disorders: Public health challenges, [Online]. Available: [Link](https://www.who.int/mental_health/neurology/neurological_disorders_report_web.pdf)
[^2]: A. A. S. LLC. (2012). Amputee statistics you ought to know, [Online]. Available: [Link](https://advancedamputees.com/amputee-statistics-you-ought-know)
[^3]: [Khan Academy: Neuromuscular Junction](https://www.khanacademy.org/science/health-and-medicine/human-anatomy-and-physiology/introduction-to-muscles/v/neuromuscular-junction)
[^4]: [Wikipedia: Hodgkin Huxley Model](https://en.wikipedia.org/wiki/Hodgkin%E2%80%93Huxley_model)
[^5]: R. Bertram. (2008). Models of calcium-induced neuro-transmitter release, [Online]. Available: [Link](https://www.math.fsu.edu/~bertram/presentations/workshops/MBI_08/transmitter.pdf)
[^6]: F. Luo, M. Dittrich, J. R. Stiles, and S. D. Meriney, “Single-pixel optical fluctuation analysis of calcium channel function in active zones of motor nerve terminals,” Journal of Neuroscience, vol. 31, no. 31, pp. 11 268–11 281, Aug. 2011. doi: 10.1523/jneurosci.1394-11.2011. [Online]. Available: [Link](https://doi.org/10.1523/jneurosci.1394-11.2011)
[^7]: M. Dittrich, A. E. Homan, and S. D. Meriney, “Presynaptic mechanisms controlling calcium-triggered transmitter release at the neuromuscular junction,”Curr Opin Physiol, vol. 4, pp. 15–24, Aug. 2018.
[^8]: M. Huss, A. Lansner, P. Wallen, A. El Manira, S. Grillner, and J. H. Kotaleski, “Roles of ionic currents in lamprey CpG neurons: a modeling study,” J. Neurophysiol., vol. 97, no. 4, pp. 2696–2711, Apr. 2007.
[^9]: F. A. Dodge and R. Rahamimoff, “Co-operative action a calcium ions in transmitter release at the neuromuscular junction,” J. Physiol. (Lond.), vol. 193, no. 2, pp. 419–432, Nov. 1967.
[^10]: A. Destexhe, Z. F. Mainen, and T. J. Sejnowski, “Synthesis of models for excitable membranes, synaptic transmission and neuromodulation using a common kinetic formalism,” J Comput Neurosci, vol. 1, no. 3, pp. 195–230, Aug. 1994.
[^11]: [Wikipedia: Diffusion Equation](https://en.wikipedia.org/wiki/Diffusion_equation)
[^12]: [Wikipedia: Michaelis-Menten Kinetics](https://en.m.wikipedia.org/wiki/Michaelis%E2%80%93Menten_kinetics)
[^13]: T. Naka, K. Shiba, and N. Sakamoto, “A two-dimensional compartment model for the reaction-diffusion system of acetylcholine in the synaptic cleft at the neuromuscular junction,” BioSystems, vol. 41, no. 1, pp. 17–27, 1997.
[^14]: D. L. Y. Wang, “Spectral element simulation of reaction-diffusion system in the neuromuscular junction,” Journal of Applied & Computational Mathematics, vol. 02, no. 04, 2013. doi: 10 . 4172 / 2168 - 9679.1000136. [Online]. Available: https://doi.org/10.4172/2168-9679.1000136
