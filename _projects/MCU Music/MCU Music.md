---
name: Microcontroller Music Scores
tools: [arm7, chiptunes]
image: /assets/mdimg/MCUMusic_Cover.jpg
category: Academic
description: Generating multi-tone digital music waveforms on a piezoelectric speaker in ARM assembly.
title: Making Music in Micro-controller Assembly
cardorder: 3
---

![TopVisual](mdimg/TopVisual-MCU.jpg)

---

Playing music digitally can be approached from two different directions: either by **1)** replaying sampled audio waveforms, or **2)** synthesizing a new waveform based on a musical score.  The former is found in _wav_, _mp3_, _ogg_, and similar formats that capture physical recordings or highly produced audio, while the latter is often packaged as _midi_, _xm_, or a myriad of formats and are used for music in video games, retro chiptunes, and music composition programs.  Score-based audio is preferable in many cases, especially when the available storage space and hardware budget are low.

As a final assignment for a microcontroller development course, this software project was created, that featured the music scoring and multi-tone generation capabilities necessary to play the theme song from a classic platformer game, _Megaman 2_ on the NES, written entirely in ARM7 assembly, using only a single piezospeaker! 

_This article explains the audio and design principles used to implement such a program._

> <i class="fa fa-music"></i>&nbsp; [Side-by-side Original and Micro-controller versions of _Megaman 2 - Title Screen_](#the-performance)

---

## Intro to Chiptunes

<figure>
<img src="mdimg/chiptuneswiki.jpg" width="100%">
<figcaption class="figure-caption text-center">Two Nintendo Gameboys connected to an audio mixer to generate live chiptune music. <a href="https://en.wikipedia.org/wiki/Chiptune">Image source</a></figcaption>
</figure>

In a poetic way, music is often said to be the synthesis of the art and mathematics.  If that description is acceptable, then [chiptunes](https://en.wikipedia.org/wiki/Chiptune) are the synthesis of pure musical _composition_ and electrical engineering.

For the final project of a microcontroller course, we were given the task to use some aspect of our ARM development board in a unique way.  Being a fan of retro video games, where the chiptunes genre was born, I decided to write a program that could be used to compose and play chiptunes on the board, specifically the Megaman 2 theme song.

## Generating Tones

Most early digital music utilized minimalistic audio chips called programmable sound generators (PSGs) that could usually only generate several basic waveforms. sinusoidal, triangle, sawtooth, noise, or square.  However, the _Embedded Artists LPC2148 Development Board_ for the class did not even have this capability, having only a single digital pin connected to a small piezospeaker for audio output!

This meant the only tone possible to generate was a full-amplitude square wave, by toggling the digital output at a desired frequency.

<figure>
<img src="mdimg/pianofreqs.jpg" width="75%">
<figcaption class="figure-caption text-center" align="center">Audio wave frequencies shown above the corresponding piano keys and note names. <a href="https://learn.parallax.com/tutorials/robot/cyberbot/sound-cyberbot/notes-frequencies">Image Source</a></figcaption>
</figure>

To simplify frequency representation, the possible output tones were limited to the notes of a chromatic scale (or all possible keys on a piano), since this is the smallest interval of quantization used in most tonal music around the world.  The diagram above shows the classical Western note names and the frequencies to which they are tuned.

## Faking Polyphony

However, modulating frequency alone, we can only play one note at a time!  Nearly all music involves at least some polyphony, or playing chords or multiple notes at once, to add depth and harmony to the music. 

<figure>
<img src="mdimg/waveinterference2.jpg" width="75%">
<figcaption class="figure-caption text-center" align="center">Illustration of two simultaneous notes interference waveform. <a href="https://courses.lumenlearning.com/boundless-physics/chapter/interactions-with-sound-waves/">Image Source</a></figcaption>
</figure>

Just looking at the audio waveform when two notes are played simultaneously, we can see that constructive and destructive *amplitude interference* is present clearly in the output waveform, which is a major problem, because the speaker on the board is controlled by a *digital* pin which can only have one amplitude: 0 to 1!

<figure>
<img src="mdimg/arpeggiation.jpg" width="60%">
<figcaption class="figure-caption text-center" align="center">
A fixed-amplitude square wave cannot play harmonic chords (top), but can play arpeggiated chords (bottom) rapidly to create an auditory illusion of multiple notes. 
<a href="https://dictionary.onmusic.org/appendix/topics/broken-chords">Image Source</a></figcaption>
</figure>

Because of this limitation, another trickier technique was used to achieve polyphony by leveraging a feature of human hearing.  If the individual notes of a chord are played fast enough in a repeating sequence (arpeggiation), the resulting sound is very similar to if they were all played together.  

<figure>
<img src="mdimg/Thaumatrope.gif">
<figcaption class="figure-caption text-center" align="center">
A gif version of what a thaumatrope toy looks like when spun rapidly. 
<a href="https://en.wikipedia.org/wiki/Thaumatrope">Image Source</a></figcaption>
</figure>

A visual analogy for how this works is a *thaumatrope*, a 18th century toy where a doublesided picture was pasted to a dowel, that could be spun quickly between two outstretched hands.  Staring at the center of the spinning paper disk creates the illusion that both pictures are actually visible at the same time, simulated by the gif above.

## Orchestrating the Code

Fundamentally, music scores are nothing more that an ordering of notes and rests, each of which is played at a certain time and for a certain duration, determined by a tempo and time signature.

Unconcerned with formal music theory, a minimal 32-bit format was designed to representing playing zero to three notes for some duration of time.

> **32 Bit Format**
> 
> | reserved | delay | note2 | octave2 | note1 | octave1 | note0 | octave0 | #notes |
> | -------- | ----- | ----- | ------- | ----- | ------- | ----- | ------- | ------ |
> | 31       | 30-23 | 22-19 | 18-16   | 15-12 | 11-9    | 8-5   | 4-2     | 1-0    |
> 
> **Bit Assignment**
> 
> | Field         | Description                                                                                                                                         |
> | ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
> | **reserved**: | Always set as zero. used by ripple chord timer                                                                                                      |
> | **delay**:    | How many units of set time expire for this note/chord (Q3)                                                                                          |
> | **noteX**:    | Note within octave (low to high notes)                                                                                                              |
> | **octaveX**:  | Particular octave from (low to high notes)                                                                                                          |
> | **#notes**:   | <span>Number of notes played (or if rest) for the duration<br>0: rest (no sound)<br>1: single<br>  2: double-stop (two notes)<br>  3: chord </span> |

Essentially, the organization of the project is based around three separate timing intervals: 1) the period of tone generation, 2) the arpeggiation sequencing, and 3) the overall note order and duration.  To draw a parallel with "proper" chiptune generation, the first two cycles would be built into the PSG chip itself, and the third would be coded as part of the video game/note composer program.

Fortunately, the NXP LPC2148 microcontroller driving the board had two built-in [timer peripherals](https://developer.arm.com/documentation/102379/0000/What-is-the-Generic-Timer-), so the majority note and chord generation involved properly enabling them, setting their periods properly, and defining their interrupt behavior.  The remaining code simply tracked time and position in the score, and kept playing it continuously in a loop.  A compatible binary was compiled and flashed to the MCU using Keil's MicroVision IDE software.

The complete project source consisted of only 4 files totaling 22.8 kB that compiled into a microcontroller binary file that was only 5.4 kB, of which the song score itself only counted for an unbelievably small 256 bytes of data!

## The Performance

Finally the wait is over for some actual music, here we have two _mp3_ recordings of the _Megaman 2 - Title Screen_ theme, played on the original NES chip, and then on the development board project.  Because of development time constraints, only the first half of the song (0:00 - 0:20) was transcribed.

**The Original:**

<audio src='2%20-%20Title%20Screen.mp3' controls><a href='2%20-%20Title%20Screen.mp3'>2 - Title Screen</a></audio>

| Device/Chipset                                                                                                              | MP3 File Size | Music Size                                                      | # Audio Channels |
| --------------------------------------------------------------------------------------------------------------------------- | ------------- | --------------------------------------------------------------- | ---------------- |
| [NES](https://en.wikipedia.org/wiki/Nintendo_Entertainment_System) / [Ricoh_2A03](https://en.wikipedia.org/wiki/Ricoh_2A03) | 708.8 kB      | 16.1 kB (`.nsf` file, 24 songs)<br>~688 bytes (1 song estimate) | 4                |

**The Micro-controller Tribute:**

<audio src='mcu_megaman.mp3' controls><a href='mcu_megaman.mp3'>mcu_megaman</a></audio>

| Device/Chipset                                                                                              | MP3 File Size | Music Size                                              | # Audio Channels |
| ----------------------------------------------------------------------------------------------------------- | ------------- | ------------------------------------------------------- | ---------------- |
| [LPC2148 Board](https://www.digikey.bg/htmldatasheets/production/854416/0/0/1/lpc2148-education-board.html) | 645.1 kB      | 256 bytes (song score only),<br>5.4kB (song + PSG code) | 1                |

Perhaps amusingly, the MCU version sounds very much like an early cell phone ring tone interpretation of the original.  This isn't far from reality, since many phones from the 90s used piezoelectric speakers to save space.  For both versions, the file size difference between the _recorded_ (`.mp3`) and _synthesized_ (`.nsf`, _custom format_) implementations is roughly 1,000 times smaller!

Personally, the project's reusability is its best feature: any song can be played by transcribing the notes into the 32-bit format shown (using ARM assembly's `DCD` to store them in accessible memory), and then simply looping through those addresses and branching to the `play_line` routine.

In summation, programming in a limited space is an excellent exercise in creative minimalism, and demonstrates the capability of microcomputing.
