# Ben Companeitz - Personal Website

A showcase for personal and academic projects, along with an overview of engineering experience.

Built using the Jekyll theme [portfolYOU by Youssef Raafat Nasry](https://github.com/YoussefRaafatNasry/portfolYOU).

