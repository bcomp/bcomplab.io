---
name: PlaceHolder Project
title: PlaceHolder Project Title
tools: [nothing, important]
image: /assets/mdimg/AbstractLines.png
description: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
mathjax: true
---

# PlaceHolder Project

Constructing a sentence that represents futher writing in a project article requires the understanding that readers often pull from an experience set that directly pertains to the comprehension of article-based knowledge in the form of a website.

Understanding the psychology of auto-generated paragraph forms in the communicative purpose of deconstructing essentialized blog-centric strategies is underpined by multiple conceits regarding the ethos of tech-literacy, aggregate attention span, and cushion ergonomics.

![I can't describe this picture to a blind person](/assets/mdimg/AbstractLines.png)

Such graphics connote the ever-changing, rhisomal qualities of transmitted visual imagery and the limited capacity we possess to disentangle alternative interpretations from the integral pathology of trans-digitally operated touchscreen interfaces. 

[A link to my nobel prize.](https://wikipedia.org)

Calculating interdimensional cost quotients with $0.01 < \mu < 1\time 10^{100}$ yields irreconcilable convergence outcomes for initial conditions even one epsilon away.

$$
\begin{equation}
\sum^{n}_{i=1} i = \frac{1}{2} n (n + 1)
\end{equation}
$$
