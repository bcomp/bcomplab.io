---
name: Postfile Project
title: Postfile Plugin Project
tools: [Jekyll, Jekyll-Plugins, Web]
image:
description: Natural image file placement
---

By using the `jekyll-postfile` plugin, image files can be placed in the same directory as the markdown and content files that reference them.

![Placeholder picture](AbstractLines_Landscape.png)
