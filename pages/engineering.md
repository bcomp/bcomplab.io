---
layout: page
title: Engineering
permalink: /engineering/
weight: 1
---

# **Engineering Experience**

## Academic

Ben has his master's degree in _electrical engineering_ and bachelor's in _computer engineering_ from Cal State University, Northridge.  He has a passion for applying system control and analysis techniques to low-level software and hardware design.

During his academic career, he studied a broad range of topics, including: control theory, state-space system analysis, and VLSI hardware design, with experience in embedded software development, robotic firmware architecture, and electrical analysis. 

<div class="row">
{% include about/timeline_school.html %}
</div>
<br>

## Professional

Ben contributed to several on-going tasks at NASA's Jet Propulsion Lab as a year-round intern.  During his most recent robotics task, he refined several skills, such as low-level firmware design and surface-mount soldering, and learned several new ones, including limited mechanical design and using several varieties of 3D-printers.

<div class="row">
{% include about/timeline_work.html %}
</div>
<br>

## Skills

Having a long history of academic and self-education, Ben is comfortable with a wide variety of engineering tasks ranging from mathematical analysis, to electromechanical fabrication and design, to programming.

<div class="row">
{% include about/skills.html title="Engineering Software" source=site.data.software-skills %}{% include about/skills.html title="Programming Skills" source=site.data.programming-skills %}
{% include about/skills.html title="Other Skills" source=site.data.other-skills %}
</div>

