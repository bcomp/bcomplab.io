---
layout: page
title: About
permalink: /about/
weight: 3
---

## About Ben

When he is not busy with engineering projects, Ben enjoys himself both in and outdoors and can be found playing games with friends as much as traveling and hiking the South-Western landscape.

Some of his recent pandemic-inspired interests include discovering [cyberpunk fiction](https://cyberpunkdatabase.net/bsearch.php?query=gibson&column=all), and pretending [xerophytic](https://en.wikipedia.org/wiki/Xerophyte) gardening requires skill. 

Having abandoned traditional social media several years ago, Ben is best accessible via [Matrix chat]({{ site.data.social-media.matrix.url }}{{ site.author.matrix }}) or [email]({{ site.data.social-media.email.url }}{{ site.author.email }}), but in-person conversation is his favorite medium.

{% capture carousel_images %}
/assets/img/bryce_self_portrait.jpg
/assets/img/zion_landscape.jpg
{% endcapture %}
{% include elements/carousel.html %}

## About This Site

The purpose of this site is to introduce Ben's professional and academic pursuits, and to serve as a continually updating platform to share his ongoing projects and work that aligns with the principles of _self-curation_ and _digital minimalism_. 

For the technically curious, the site was built using the [Jekyll](https://jekyllrb.com/) framework, and styled with a modified version of the excellent Jekyll theme [portfolYOU](https://github.com/YoussefRaafatNasry/portfolYOU).


