---
layout: default
blurb: Electrical engineer and embedded software developer, interested in control theory and system modeling.
permalink: /
---

{% include landing.html %}
