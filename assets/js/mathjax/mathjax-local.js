MathJax = { 
  loader: {
    load: ['[tex]/ams']
  },
  tex: {
    packages: ['base', 'ams'], // extensions to use
    inlineMath: [              // start/end delimiter pairs for in-line math
      ['\\(', '\\)'],
      ['$', '$']
    ],  
    displayMath: [             // start/end delimiter pairs for display math
      ['$$', '$$'],
      ['\\[', '\\]']
    ],  
    processEscapes: true,      // use \$ to produce a literal dollar sign
    processEnvironments: true, // process \begin{xxx}...\end{xxx} outside math mode
    processRefs: false,         // process \ref{...} outside of math mode
    tags: 'none',              // or 'ams' or 'all'
    tagSide: 'right',          // side for \tag macros
    tagIndent: '0.8em',        // amount to indent tags
    useLabelIds: true,         // use label name rather than tag for ids
    maxMacros: 1000            // maximum number of macro substitutions per expression

  }
};

